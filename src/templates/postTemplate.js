import React from "react"
import { graphql } from "gatsby"
import { MDXProvider } from "@mdx-js/react"
import { MDXRenderer } from "gatsby-plugin-mdx"
import Layout from '../components/layout'

export default function PageTemplate({ data: { mdx } }) {
  return (
    <Layout>
      <div className="blog-post-container">
        <div className="blog-post">
          <h1>{mdx.frontmatter.title}</h1>
          <div className="blog-post-content">
            <MDXProvider>
              <MDXRenderer>{mdx.body}</MDXRenderer>
            </MDXProvider>
          </div>
          <time>{mdx.frontmatter.date}</time>
        </div>
      </div>
    </Layout>
  )
}

export const pageQuery = graphql`
  query BlogPostQuery($id: String) {
    mdx(id: { eq: $id }) {
      id
      body
      frontmatter {
        date
        title
      }
    }
  }
`
